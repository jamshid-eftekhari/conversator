﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ConservatorWin10App.Annotations;
using ConservatorWin10App.Common;
using ConservatorWin10App.Model;

namespace ConservatorWin10App.ViewModel
{
    class SculptureViewModel : INotifyPropertyChanged
    {
        public SculptureCatalog SculptureCatalog { get; set; }

        private string _name;
        private string _address;
        private string _sculptureType;
        private ICommand _sculptureSelected;
        private Sculpture _selectedSculpture;
        private int _selectedIndex;
        private string _materialName;
        private ICommand _createSculpture;
        private string _elementBackgroundColor = "Red";
        public int ListCount { get; set; }
        public int Adding { get; set; }


        public string ElementBackGroundColor
        {
            get { return _elementBackgroundColor; }
            set { _elementBackgroundColor = value; OnPropertyChanged(); }
        }
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { _selectedIndex = value; OnPropertyChanged(); }
        }

        public string MaterialName
        {
            get { return _materialName; }
            set { _materialName = value; OnPropertyChanged(); }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        public string Address {
            get { return _address; }
            set { _address = value; OnPropertyChanged(); }
        }

        public string SculptureType
        {
            get { return _sculptureType; }
            set { _sculptureType = value; OnPropertyChanged(); }
        }

        public ICommand SculptureSelected
        {
            get
            {

                return _sculptureSelected ?? (_sculptureSelected= new RelayArgCommand<Sculpture>(ev=>SculptureCatalog.SetSelectedSculpture(ev)));
            }
            set
            {
                _sculptureSelected = value;
                
                OnPropertyChanged();
            }
        }
        public ICommand CreateSculpture
        {
            get
            {
                if (_createSculpture == null)
                    _createSculpture = new RelayCommand(HandleCreateSculpture);
                return _createSculpture;
            }
            set
            {
                _createSculpture = value;
            }
        }

        public void HandleCreateSculpture()
        {
            //ElementBackGroundColor = "Green";
            Adding++;
            SculptureCatalog.AddSculpture("test_sculpture", "Test_address");
        }

        public Sculpture SelectedSculpture
        {
            get { return _selectedSculpture; }
            set { _selectedSculpture = value; OnPropertyChanged(); }
        }
        public string InspectionDate { get; set; }

        public SculptureViewModel()
        {
            SculptureCatalog = SculptureCatalog.Instance;
            SculptureCatalog.ReceiveVMreference(this);
        }



        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
