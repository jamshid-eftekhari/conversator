﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConservatorWin10App.Model
{
    class Inspection
    {
        public int Frequency { get; set; }
        public DateTime InspectionDate { get; set; }
        public ObservableCollection<Damage> Damages { get; set; }
    }
}
