﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ConservatorWin10App.Model
{
    class Sculpture
    {
      
        [JsonProperty(PropertyName = "Sculpture_Id")]
        public int Id { get; set; }
        public string Name { get; set; }
        //public string Placement { get; set; }
        //public string SculptureType { get; set; }
        public string Address { get; set; }
        //public string PicLink { get; set; }
        ////public Material SculptureMaterial { get; set; }
        //public ObservableCollection<Material> SculptureMaterials { get; set; }
        //public ObservableCollection<Inspection> Inspections { get; set; }
        public ObservableCollection<Material> Materials { get; set; }
        //public Sculpture () { }

        //public Sculpture(int id, string name, string placement, string sculptureType, string address,
        //    string picLink, /*string materialType, string materialName*/ObservableCollection<Material> materials, ObservableCollection<Inspection> inspections)
        //{
        //    Id = id;
        //    Name = name;
        //    Placement = placement;
        //    SculptureType = sculptureType;
        //    Address = address;
        //    PicLink = picLink;
        //    //SculptureMaterial = new Material {MaterialName = materialName, MaterialType = materialType};
        //    SculptureMaterials = materials;
        //    Inspections = inspections;
        //}

        //public override string ToString()
        //{
        //    //return string.Format("Id: {0},Name: {1},Plcement: {2},Type: {3},Address: {4},Material Type: {5},Material Name: {6} Inspection: {7}", Id, Name,Placement,SculptureType,Address,SculptureMaterial.MaterialName, SculptureMaterial.MaterialType, Inspections[0].InspectionDate);
        //    return string.Format("Id: {0},Name: {1},Plcement: {2},Type: {3},Address: {4},Material Type: {5},Material Name: {6} Inspection: {7}", Id, Name, Placement, SculptureMaterials[0].MaterialName, SculptureMaterials[1].MaterialName, Inspections[0].InspectionDate);

        //}
    }
}
