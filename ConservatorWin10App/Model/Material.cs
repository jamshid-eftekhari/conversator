﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Newtonsoft.Json;

namespace ConservatorWin10App.Model
{
    class Material
    {
        [JsonProperty(PropertyName = "Name")]
        public string MaterialName { get; set; }
        public string MaterialType { get; set; }

        public MaterialTypes MaterialTypes { get; set; }

        public override string ToString()
        {
            //return string.Format("Id: {0},Name: {1},Plcement: {2},Type: {3},Address: {4},Material Type: {5},Material Name: {6} Inspection: {7}", Id, Name,Placement,SculptureType,Address,SculptureMaterial.MaterialName, SculptureMaterial.MaterialType, Inspections[0].InspectionDate);
            return string.Format(MaterialName);

        }
    }
}
