﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConservatorWin10App.Model
{
    class Damage
    {
        public string DamageType { get; set; }
        public string DamageLocation { get; set; }
        public string Threatment { get; set; }
        public int Ranck { get; set; }
        public string PicLink { get; set; }
    }
}
