﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Windows.Security.Credentials.UI;
using Windows.Security.Cryptography.Core;
using ConservatorWin10App.ViewModel;

namespace ConservatorWin10App.Model
{
    class SculptureCatalog
    {
        private static SculptureCatalog _instance;
      //  private int Adding = 0;

        public HttpClient httpClient;
        public SculptureViewModel SculptureViewModel { get; set; }
        

        public static SculptureCatalog Instance
        {

            get { return _instance ?? (_instance = new SculptureCatalog()); }
        }

        public ObservableCollection<Sculpture> Sculptures { get; set; }
        public ObservableCollection<Material> Materials { get; set; }
        public ObservableCollection<Inspection> Inspections { get; set; }
        public ObservableCollection<Damage> Damages { get; set; }
        public MaterialTypes MaterialTypes { get; set; }

        public Sculpture SelectedSculpture { get; set; }
      //  public ICommand _sculptureSelected;
        private SculptureCatalog()
        {
            Sculptures = new ObservableCollection<Sculpture>();
            Materials = new ObservableCollection<Material>();
            Inspections = new ObservableCollection<Inspection>();
            Damages = new ObservableCollection<Damage>();
            MaterialTypes = new MaterialTypes();
            SelectedSculpture = new Sculpture();
            LoadSculptures();
           LoadSculpturesAsync();
        }

        //public ICommand SculptureSelected
        //{
        //    get { return _sculptureSelected; }
        //    set { _sculptureSelected = value; }
        //}

        public void ReceiveVMreference(SculptureViewModel SculptureVM)
        {
            SculptureViewModel = SculptureVM;
        }
        public void SetSelectedSculpture(Sculpture selected)
        {
           // SculptureViewModel.SelectedSculpture = selected;
            SelectedSculpture = selected;
           // LoadSculpturesAsync(SelectedSculpture.Id);
        }

        //public void LoadSculptures()
        //{
        //    Damages.Add(new Damage { DamageLocation = "Socket", DamageType = "Bio Growth", PicLink = "link", Ranck = 2, Threatment = "requiret" });
        //    Inspections.Add(new Inspection { Damages = Damages, Frequency = 10, InspectionDate = new DateTime(2015, 1, 6, 9, 0, 0) });
        //    Sculptures.Add(new Sculpture(1, "Yngling til Hest", "Ground", "sockel", "Rødkilde Plads",
        //                   "ms-appx:///Assets/s2.jpg", "sten", "granit", Inspections));
        //    Sculptures.Add(new Sculpture(2, "Arkæologen Georg Zoëgat", "Ground", "sockel", "Glyptotekets anlæg ved Tietgensgade",
        //                  "ms-appx:///Assets/s3.jpg", "sten", "granit", Inspections));
        //    Sculptures.Add(new Sculpture(3, "Mindetavle for Københavns belejring 1658-60", "Ground", "sockel", "på Nationalmuseets mur under søjlegalleriet mod Vester Voldgade",
        //                  "ms-appx:///Assets/s4.jpg", "sten", "granit", Inspections));
        //    Sculptures.Add(new Sculpture(4, "Maleren Asmus Jacob Carstens", "Ground", "sockel", "Glyptotekets anlæg ved Niels Brocksgade",
        //                  "ms-appx:///Assets/s5.jpg", "sten", "granit", Inspections));
        //    Sculptures.Add(new Sculpture(4, "Maleren Asmus Jacob Carstens", "Ground", "sockel", "Glyptotekets anlæg ved Niels Brocksgade",
        //                 "ms-appx:///Assets/s6.jpg", "sten", "granit", Inspections));
        //}

        public void LoadSculptures()
        {
            //Damages.Add(new Damage { DamageLocation = "Socket", DamageType = "Bio Growth", PicLink = "link", Ranck = 2, Threatment = "requiret" });
            //Inspections.Add(new Inspection { Damages = Damages, Frequency = 10, InspectionDate = new DateTime(2015, 1, 6, 9, 0, 0) });
            //Materials.Add(new Material { MaterialName = "jern", MaterialType = "Metal" });
            //Sculptures.Add(new Sculpture(1, "Yngling til Hest", "Ground", "sockel", "Rødkilde Plads",
            //               "ms-appx:///Assets/s2.jpg", Materials, Inspections));
            //Sculptures.Add(new Sculpture(2, "Arkæologen Georg Zoëgat", "Ground", "sockel", "Glyptotekets anlæg ved Tietgensgade",
            //              "ms-appx:///Assets/s3.jpg", Materials, Inspections));
            //Sculptures.Add(new Sculpture(3, "Mindetavle for Københavns belejring 1658-60", "Ground", "sockel", "på Nationalmuseets mur under søjlegalleriet mod Vester Voldgade",
            //              "ms-appx:///Assets/s4.jpg", Materials, Inspections));
            //Sculptures.Add(new Sculpture(4, "Maleren Asmus Jacob Carstens", "Ground", "sockel", "Glyptotekets anlæg ved Niels Brocksgade",
            //              "ms-appx:///Assets/s5.jpg", Materials, Inspections));
            //Sculptures.Add(new Sculpture(4, "Maleren Asmus Jacob Carstens", "Ground", "sockel", "Glyptotekets anlæg ved Niels Brocksgade",
            //             "ms-appx:///Assets/s6.jpg", Materials, Inspections));
        }

        public async void LoadSculpturesAsync()
        {
           GetSculptures();

            //var Sculptures = await LoadEventsFromJsonAsync();
            //if (Sculptures != null)
            //    foreach (var ev in Sculptures)
            //    {
            //        Sculptures.Add(ev);
            //    }
            //else
            //{
            //    //Data til testformål
                
            //}
        }

        public async void LoadSculpturesAsync(int id)
        {
            GetSculptures(id);

        }

        public async void GetSculptures()
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44444");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync("api/sculptures");
                if (response.IsSuccessStatusCode)
                {


                    //var result = response.Content.ReadAsStringAsync();

                    //var result = response.Content.ReadAsAsync<Sculpture>().Result;
                    var result = response.Content.ReadAsAsync<List<Sculpture>>().Result;

                    Sculptures.Clear();

                    // Sculptures.Add(result);
                    if(Instance.SculptureViewModel.Adding == 0)
                        Instance.SculptureViewModel.ListCount = result.Count;
                    int i = 0;
                    foreach (var sculp in result)
                    {
                        Sculptures.Add(sculp);
                        if (i >= SculptureViewModel.ListCount)
                            SculptureViewModel.ElementBackGroundColor = "Gray";
                        i++;
                    }

                }




            }


            //httpClient = new HttpClient();
            //httpClient.BaseAddress = new Uri("http://localhost:44444");
            //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //httpClient.MaxResponseContentBufferSize = 2660000;

            //var response = await httpClient.GetAsync("api/sculptures");
            //if (response.IsSuccessStatusCode)
            //{
            //    var sculpture = await response.Content.ReadAsStringAsync();
            //    var scu = JsonArray.Parse(sculpture);

            //    var qry = (from m in scu
            //        select new Sculpture()
            //        {
            //            Name = m.GetObject()["Name"].GetString(),
            //            Address = m.GetObject()["Address"].GetString(),
            //            SculptureType = m.GetObject()["PlacementType_Id"].GetNumber().ToString(),
            //        //    SculptureMaterials = m.GetObject()["Materials"].GetArray().ToList()
            //        }).ToList();
            //    var results = qry;
            //    foreach (var ev in results)
            //    {
            //        Sculptures.Add(ev);
            //    }
            //    //Sculptures = new ObservableCollection<Sculpture>(results);

            //}
           
        }

        public async void GetSculptures(int id)
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:44444");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.MaxResponseContentBufferSize = 266000;

            var response = await httpClient.GetAsync("api/sculptures/id");
            if (response.IsSuccessStatusCode)
            {
                var material = await response.Content.ReadAsStringAsync();
                var mater = JsonArray.Parse(material);

                var qry = (from m in mater
                           select new Material()
                           {
                               MaterialName = m.GetObject()["Name"].GetString(),
                               MaterialType = m.GetObject()["Address"].GetString(),                             
                           }).ToList();
                var results = qry;
                foreach (var ev in results)
                {
                    Materials.Add(ev);
                }
              

            }

        }

        public async void AddSculpture(string name, string address)
        {
            Sculpture sculpture = new Sculpture {Name = name, Address = address};
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:44444");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsJsonAsync("api/sculptures", sculpture);
                if (response.IsSuccessStatusCode)
                {
                   // Adding++;
                    GetSculptures();

                }
            }
        }

        //public static async Task<List<Sculpture>> LoadEventsFromJsonAsync()
        //{
        //    const string ServerUrl = "http://localhost:38479";
        //    HttpClientHandler handler = new HttpClientHandler();
        //    handler.UseDefaultCredentials = true;

        //    using (var client = new HttpClient(handler))
        //    {
        //        client.BaseAddress = new Uri(ServerUrl);
        //        client.DefaultRequestHeaders.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        try
        //        {
        //            var responce = client.GetAsync("api/sculptures").Result;
        //            if (responce.IsSuccessStatusCode)
        //            {
        //                var eventdata = responce.Content.ReadAsStringAsync()<IEnumerable<Sculpture>>().Result;
        //                return eventdata.ToList();
        //            }
        //            return null;
        //        }
        //        catch (Exception)
        //        {

        //            throw;
        //        }
        //    }
        //}
    }
}
