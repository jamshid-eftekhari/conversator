﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConservatorWin10App.Model
{
    class MaterialTypes
    {
        public int MaterialType_Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format(Name);
        }
    }
}
