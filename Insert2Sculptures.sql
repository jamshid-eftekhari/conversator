﻿
INSERT INTO PlacementType VALUES ('111','Ground');
INSERT INTO PlacementType VALUES ('112','Bulding');
INSERT INTO PlacementType VALUES ('113','Facade');
GO;
INSERT INTO SculptureType VALUES ('211','Sculpture');
INSERT INTO SculptureType VALUES ('212','Socket');
INSERT INTO SculptureType VALUES ('213','Relief');
INSERT INTO SculptureType VALUES ('214','WaterArt');
GO;
INSERT INTO SculptureType_Sculpture VALUES ('211','12');
INSERT INTO SculptureType_Sculpture VALUES ('212','12');
INSERT INTO SculptureType_Sculpture VALUES ('211','13');
INSERT INTO SculptureType_Sculpture VALUES ('212','13');
INSERT INTO SculptureType_Sculpture VALUES ('211','14');
INSERT INTO SculptureType_Sculpture VALUES ('212','14');
INSERT INTO SculptureType_Sculpture VALUES ('213','15');
INSERT INTO SculptureType_Sculpture VALUES ('211','16');
INSERT INTO SculptureType_Sculpture VALUES ('212','16');
INSERT INTO SculptureType_Sculpture VALUES ('211','17');
INSERT INTO SculptureType_Sculpture VALUES ('212','17');
INSERT INTO SculptureType_Sculpture VALUES ('211','18');
INSERT INTO SculptureType_Sculpture VALUES ('212','18');
INSERT INTO SculptureType_Sculpture VALUES ('211','19');
INSERT INTO SculptureType_Sculpture VALUES ('212','19');
INSERT INTO SculptureType_Sculpture VALUES ('211','20');
INSERT INTO SculptureType_Sculpture VALUES ('212','20');
INSERT INTO SculptureType_Sculpture VALUES ('211','21');
INSERT INTO SculptureType_Sculpture VALUES ('212','21');
GO;
INSERT INTO Sculpture VALUES ('111','Yngling til Hest','Rødkilde Plads');
INSERT INTO Sculpture VALUES ('111','Arkæologen Georg Zoëga','Glyptotekets anlæg ved Tietgensgade');
INSERT INTO Sculpture VALUES ('111','Kains efterkommere','Lyshøj Allé mod Toftegårds Allé');
INSERT INTO Sculpture VALUES ('112','Mindetavle for Københavns belejring 1658-60','på Nationalmuseets mur under søjlegalleriet mod Vester Voldgade');
INSERT INTO Sculpture VALUES ('111','Maleren Asmus Jacob Carstens','Glyptotekets anlæg ved Niels Brocksgade');
INSERT INTO Sculpture VALUES ('111','Drukken faun','Gyldenløvesgade ved Sankt Jørgens Sø');
INSERT INTO Sculpture VALUES ('111','Hestebetvinger','Bavnehøj Allé ud mod Enghavevej ');
INSERT INTO Sculpture VALUES ('111','Løve og Løvinde','Jarmers Plads');
INSERT INTO Sculpture VALUES ('111','En neapolitansk fisker lærer sin søn at spille på fløjte','I anlæg på hjørnet af Store Strandstræde og Lille Strandstræde');
INSERT INTO Sculpture VALUES ('111','Tubalkain','Sølvgade ved Botanisk Have');
GO;
INSERT INTO Notes VALUES ('12','Tilstandsbeskrivelse','2005-06-01','Bronzen fremstår primært lys grøn med sort i læflader, hvor der også er aflejringer af sorte skorper. På lodrette flader er der partielle løbespor. Overfladen er forvitret, der er synlige støbeskel og ingen synlige arbejdsspor.
Soklen fremstår generelt i fin stand, der er ingen strukturelle skader. Dog må der foretages en reetablering af fuger for at forhindre vandindsivning i revnerne mellem fuge og sten, hvilket kan medføre udvaskning og nedbrydning af fuger såvel om stenmateriale.
Fremstår med smuds og sorte skorper. Desuden aflejringer langs fugerne hvilket ses som et tegn på udvaskning.');
INSERT INTO Notes VALUES ('12','Tilsyn 2015','2005-06-01','Bronzeskulpturen fremstår primært lys grøn med få sorte øer i højt releif (overfladen er forvitret/korroderet) og partielle områder med sorte løbespor. På bagsiden på løveskindet sorte skorper. Cirkelrund rustfarvet område på højre lyske. Synlige samlinger /støbeskel. Den blankpolerede granitsokkel fremstår med sorte aflejringfer primært på de vandrette flader samt med afslag på de nederste lodrette hjørner og partelt på de lange lodrette flader. ');
INSERT INTO Notes VALUES ('13','Tilstandsbeskrivelse','2005-06-01','Sokkel fremstår med biologisk vækst i form af alger og mos. Der er forvitring i form af små gruber på hele overfladen. Overfladen er misfarvet i form af sort maling.
Fugerne er løsnede. Der er afslag af nederste hjørne mod vest. Bronzen fremstår med lys grøn til mørk grøn overflade. I læflader sort med skorper og løbespor. Forvitringen ses ved synlige støbeskel og ensartet reduktion af overfladen.
Bør indgå i behandlingsplan ellers tilses om 10 år.
');
INSERT INTO Notes VALUES ('13','Fugning','2010-10-01','Soklen fuget');
INSERT INTO Notes VALUES ('13','Tilsyn 2015','2015-08-24','Bronzen fremstår med lys grøn til mørk grøn overflade. I læflader sort med skorper/ krust og løbespor. Forvitringen ses ved synlige støbeskel og sorte øer over niveau på overfladen.
Sokkel fremstår med biologisk vækst i form af alger og mos.
Der er forvitring i form af små gruber på hele overfladen. Overfladen er misfarvet i form af sort maling og smuds.
Fugerne er er ok. Der er afslag af nederste hjørne mod vest.
Træerne rundt om, vokser indover skulpturen.
Bør indgå i behandlingsplan hvor både sokkel og skulpur afvaskes og sorte skorper, biologisk vækst samt smuds fjernes.
');
INSERT INTO Notes VALUES ('14','Tilstandsbeskrivelse','2007-03-07','Granitsokkel fremstår med biologisk vækst på hele fladen.
Bronzen fremstår lys grøn med sorte partier i læflader og sorte øer. I læflader ses ligeledes krustationer. Flere af samlingerne er revnede og der er synlige støbeskel og nitter.
Det bør overvejes om det samlede udtryk kommer skulpturen til gode eller om en generelt afrensning med retouchering af de mange farveforskelle vil være på sin plads.
');
INSERT INTO Notes VALUES ('14','Fugning','2015-06-10','Soklen fuget');
INSERT INTO Notes VALUES ('14','Tilsyn 2015','2010-10-01','Granitsokkel fremstår med biologisk vækst på hele fladen.
Bronzen fremstår lys grøn med sorte partier i læflader og sorte øer. I læflader ses ligeledes krustationer. Flere af samlingerne er revnede og der er synlige støbeskel og nitter. På tæerne af den midterste skulpturs fod er der biologisk vækst i form af alger oven på sorte skorper.
Det bør overvejes om det samlede udtryk kommer skulpturen til gode eller om en generelt afrensning med retouchering af de mange farveforskelle vil være på sin plads.
Anbefalningen er at skulpturen behandles med henblik på at opnå et mere ensartet udtryk. Revner bør ligeledes udbedres. Kan indgå i en behandlingsplan for behandling af bronzer.');
INSERT INTO Notes VALUES ('15','Tilstandsbeskrivelse','2008-08-05','SFremstår meget smudset med misfarvninger fra bronzebogstaver (oprindelig blank?). Bronzebogstaverne fremstår mat sort, med korrosionsprodukter og smuds.');
INSERT INTO Notes VALUES ('15','Fugning','2015-06-10','Soklen fuget');
INSERT INTO Notes VALUES ('16','Titleeeeee','2005-03-15','Discriptionnnnnnn');
INSERT INTO Notes VALUES ('16','Titleeeeee','2015-06-10','SDiscriptionnnnnnn');
INSERT INTO Notes VALUES ('17','Titleeeeee','2005-03-15','Discriptionnnnnnn');
INSERT INTO Notes VALUES ('17','Titleeeeee','2015-06-10','SDiscriptionnnnnnn');
INSERT INTO Notes VALUES ('18','Titleeeeee','2005-03-15','Discriptionnnnnnn');
INSERT INTO Notes VALUES ('18','Titleeeeee','2015-06-10','SDiscriptionnnnnnn');
INSERT INTO Notes VALUES ('19','Titleeeeee','2005-03-15','Discriptionnnnnnn');
INSERT INTO Notes VALUES ('19','Titleeeeee','2015-06-10','SDiscriptionnnnnnn');
INSERT INTO Notes VALUES ('22','Titleeeeee','2005-03-15','Discriptionnnnnnn');
INSERT INTO Notes VALUES ('22','Titleeeeee','2015-06-10','SDiscriptionnnnnnn');
INSERT INTO Notes VALUES ('21','Titleeeeee','2005-03-15','Discriptionnnnnnn');
INSERT INTO Notes VALUES ('21','Titleeeeee','2015-06-10','SDiscriptionnnnnnn');


INSERT INTO MaterialTypes VALUES ('Stone');
INSERT INTO MaterialTypes VALUES ('Metal');
INSERT INTO MaterialTypes VALUES ('Other');

INSERT INTO Materials VALUES ('1','Granit');
INSERT INTO Materials VALUES ('1','SandSten');
INSERT INTO Materials VALUES ('1','Limestone');
INSERT INTO Materials VALUES ('1','Marmor');
INSERT INTO Materials VALUES ('1','Other montain stones');
INSERT INTO Materials VALUES ('2','Bronze');
INSERT INTO Materials VALUES ('2','Cortenstel');
INSERT INTO Materials VALUES ('2','Bemalet Stel');
INSERT INTO Materials VALUES ('2','Aluminum');
INSERT INTO Materials VALUES ('2','Iron');
INSERT INTO Materials VALUES ('2','Other Metal');
INSERT INTO Materials VALUES ('3','Wood');
INSERT INTO Materials VALUES ('3','Tegl');
INSERT INTO Materials VALUES ('3','Beton');
INSERT INTO Materials VALUES ('3','Other');

INSERT INTO SkulptureMaterial VALUES ('1','12');
INSERT INTO SkulptureMaterial VALUES ('6','12');
INSERT INTO SkulptureMaterial VALUES ('1','13');
INSERT INTO SkulptureMaterial VALUES ('6','13');
INSERT INTO SkulptureMaterial VALUES ('1','14');
INSERT INTO SkulptureMaterial VALUES ('6','14');
INSERT INTO SkulptureMaterial VALUES ('4','15');
INSERT INTO SkulptureMaterial VALUES ('6','15');
INSERT INTO SkulptureMaterial VALUES ('1','16');
INSERT INTO SkulptureMaterial VALUES ('2','16');
INSERT INTO SkulptureMaterial VALUES ('3','16');
INSERT INTO SkulptureMaterial VALUES ('4','16');
INSERT INTO SkulptureMaterial VALUES ('5','16');
INSERT INTO SkulptureMaterial VALUES ('6','16');
INSERT INTO SkulptureMaterial VALUES ('1','17');
INSERT INTO SkulptureMaterial VALUES ('6','17');
INSERT INTO SkulptureMaterial VALUES ('1','18');
INSERT INTO SkulptureMaterial VALUES ('6','18');
INSERT INTO SkulptureMaterial VALUES ('1','19');
INSERT INTO SkulptureMaterial VALUES ('6','19');
INSERT INTO SkulptureMaterial VALUES ('1','22');
INSERT INTO SkulptureMaterial VALUES ('6','22');
INSERT INTO SkulptureMaterial VALUES ('14','22');
INSERT INTO SkulptureMaterial VALUES ('1','21');
INSERT INTO SkulptureMaterial VALUES ('6','21');


INSERT INTO DamageType VALUES ('Biological growth');
INSERT INTO DamageType VALUES ('Efflorescence');
INSERT INTO DamageType VALUES ('Construction damage');
INSERT INTO DamageType VALUES ('Defective grooves');
INSERT INTO DamageType VALUES ('Discoloration');
INSERT INTO DamageType VALUES ('Cracks');
INSERT INTO DamageType VALUES ('Lost Parts');
INSERT INTO DamageType VALUES ('Other damages');

INSERT INTO DamageLocation VALUES ('Sculpture');
INSERT INTO DamageLocation VALUES ('Base');
INSERT INTO DamageLocation VALUES ('Stone');
INSERT INTO DamageLocation VALUES ('Bronze');

INSERT INTO Damages VALUES ('1','12','2');
INSERT INTO Damages VALUES ('2','12','2');
INSERT INTO Damages VALUES ('2','12','1');
INSERT INTO Damages VALUES ('5','12','2');
INSERT INTO Damages VALUES ('5','12','1');
INSERT INTO Damages VALUES ('6','12','2');

INSERT INTO Damages VALUES ('1','13','2');
INSERT INTO Damages VALUES ('2','13','1');
INSERT INTO Damages VALUES ('4','13','3');
INSERT INTO Damages VALUES ('5','13','1');

INSERT INTO CareType VALUES ('Requiring treatment (urgent) ');
INSERT INTO CareType VALUES ('Treatment on going');
INSERT INTO CareType VALUES ('10 year inspection');

INSERT INTO CareFrequency VALUES ('Each year');
INSERT INTO CareFrequency VALUES ('Each 2. year');
INSERT INTO CareFrequency VALUES ('Each 3. year');
INSERT INTO CareFrequency VALUES ('Each 4. year');
INSERT INTO CareFrequency VALUES ('Each 5. year');
INSERT INTO CareFrequency VALUES ('Not defined');


INSERT INTO Care VALUES ('12','6','3','2015-11-01','2025-11-01','2025-11-01');
INSERT INTO Care VALUES ('13','6','2','1990-01-01','1990-01-01','2025-11-01');
INSERT INTO Care VALUES ('13','5','3','1990-01-01','1990-01-01','2025-08-01');
INSERT INTO Care VALUES ('14','5','2','1990-01-01','1990-01-01','2025-06-01');
INSERT INTO Care VALUES ('15','6','3','1990-01-01','1990-01-01','2025-08-01');
INSERT INTO Care VALUES ('15','6','2','1990-01-01','1990-01-01','2025-08-01');