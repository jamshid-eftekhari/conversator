﻿CREATE TABLE Sculpture(
     Sculpture_Id  int  IDENTITY NOT NULL PRIMARY KEY,
	 PlacementType_Id  int,
     Name      VARCHAR(50)     NOT NULL,
     Address   VARCHAR(80)  NOT NULL,
	 FOREIGN KEY(PlacementType_Id) REFERENCES PlacementType(PlacementType_Id)
);

CREATE TABLE SculptureType(
     SculptureType_Id  int  NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);

CREATE TABLE SculptureType_Sculpture(
     SculptureType_Id  int,
	 Sculpture_Id  int,
	 FOREIGN KEY(SculptureType_Id) REFERENCES SculptureType(SculptureType_Id),
	 FOREIGN KEY(Sculpture_Id) REFERENCES Sculpture(Sculpture_Id),
	 PRIMARY KEY (SculptureType_Id,Sculpture_Id) 
);

CREATE TABLE PlacementType(
     PlacementType_Id  int  NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);

CREATE TABLE Notes(
     Notes_Id  int  IDENTITY NOT NULL PRIMARY KEY,
	 Sculpture_Id  int,
	 Title  VARCHAR(130)     NOT NULL,
	 NotesDate DateTime,
     NotesTxt      VARCHAR(730)     NOT NULL
	 FOREIGN KEY(Sculpture_Id) REFERENCES Sculpture(Sculpture_Id),
);

CREATE TABLE Care(
     Sculpture_Id  int,
	 CareFrequency_Id  int,
	 CareType_Id  int,
	 LastTreatment DateTime,
	 NextTreatment DateTime,
	 NextSupervision DateTime,
     FOREIGN KEY(CareFrequency_Id) REFERENCES CareFrequency(CareFrequency_Id),
	 FOREIGN KEY(CareType_Id) REFERENCES CareType(CareType_Id),
	 FOREIGN KEY(Sculpture_Id) REFERENCES Sculpture(Sculpture_Id),
	 PRIMARY KEY (Sculpture_Id,CareFrequency_Id,CareType_Id)
);

CREATE TABLE CareFrequency(
     CareFrequency_Id  int  IDENTITY NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);

CREATE TABLE CareType(
     CareType_Id  int  IDENTITY NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);

CREATE TABLE Damages(
     DamageType_Id  int,
	 Sculpture_Id  int,
	 DamageLocation_Id  int,
     FOREIGN KEY(Sculpture_Id) REFERENCES Sculpture(Sculpture_Id),
	 FOREIGN KEY(DamageType_Id) REFERENCES DamageType(DamageType_Id),
	 FOREIGN KEY(DamageLocation_Id) REFERENCES DamageLocation(DamageLocation_Id),
	 PRIMARY KEY (DamageType_Id,Sculpture_Id,DamageLocation_Id)
);

CREATE TABLE DamageType(
     DamageType_Id  int  IDENTITY NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);

CREATE TABLE DamageLocation(
     DamageLocation_Id  int  IDENTITY NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);



CREATE TABLE Materials(
     Material_Id  int  IDENTITY NOT NULL PRIMARY KEY,
	 MaterialType_Id  int,
     Name      VARCHAR(30)     NOT NULL,
	 FOREIGN KEY (MaterialType_Id) REFERENCES MaterialTypes (MaterialType_Id)
);

CREATE TABLE MaterialTypes(
     MaterialType_Id  int  IDENTITY NOT NULL PRIMARY KEY,
     Name      VARCHAR(30)     NOT NULL
);

CREATE TABLE SkulptureMaterial(
     Material_Id  int ,
	 Sculpture_Id  int,
	 FOREIGN KEY (Material_Id) REFERENCES Materials (Material_Id),
	 FOREIGN KEY (Sculpture_Id) REFERENCES Sculpture (Sculpture_Id),
     PRIMARY KEY (Material_Id,Sculpture_Id)
);

--DROP TABLE Sculpture
--DROP TABLE SculptureType
--DROP TABLE SculptureType_Sculpture