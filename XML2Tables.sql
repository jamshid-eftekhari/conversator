﻿ DECLARE @x xml

SELECT @x = MData
FROM OPENROWSET (BULK 'C:\Users\EASJ\Documents\Forår2016\SWD\final project\projects\Conservator\basis.xml', SINGLE_BLOB) AS Sculptures(MData)

SELECT @x

DECLARE @hdoc int

EXEC sp_xml_preparedocument @hdoc OUTPUT, @x

--SELECT * 
--INTO TempBasis
--FROM OPENXML (@hdoc, '/dataroot/Basis', 2)
--WITH (
--        Nummer   int , 
--        Navn     varchar(20),
--		Adresse  varchar(40)
--		)

--SELECT * 
--INTO TempBehandling
--FROM OPENXML (@hdoc, '/dataroot/Basis/Behandling', 2)
--WITH (
--        Nummer   int , 
--        Anbef_1     varchar(20),
--		Adresse  varchar(40)
--		)

EXEC sp_xml_removedocument @hdoc

--SELECT * FROM #tmp_MySubcategories
SELECT * FROM TempBehandling