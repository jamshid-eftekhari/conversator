namespace ConservatorConsoleApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Notes
    {
        [Key]
        public int Notes_Id { get; set; }

        public int Sculpture_Id { get; set; }

        [Required]
        [StringLength(130)]
        public string Title { get; set; }

        public DateTime? NotesDate { get; set; }

        [Required]
        [StringLength(730)]
        public string NotesTxt { get; set; }

        public virtual Sculpture Sculpture { get; set; }
    }
}
