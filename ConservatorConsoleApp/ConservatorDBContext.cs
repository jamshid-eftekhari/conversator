namespace ConservatorConsoleApp
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ConservatorDBContext : DbContext
    {
        public ConservatorDBContext()
            : base("name=ConservatorDBContext")
        {
        }

        public virtual DbSet<Care> Care { get; set; }
        public virtual DbSet<CareFrequency> CareFrequency { get; set; }
        public virtual DbSet<CareType> CareType { get; set; }
        public virtual DbSet<DamageLocation> DamageLocation { get; set; }
        public virtual DbSet<Damages> Damages { get; set; }
        public virtual DbSet<DamageType> DamageType { get; set; }
        public virtual DbSet<Materials> Materials { get; set; }
        public virtual DbSet<MaterialTypes> MaterialTypes { get; set; }
        public virtual DbSet<Notes> Notes { get; set; }
        public virtual DbSet<PlacementType> PlacementType { get; set; }
        public virtual DbSet<Sculpture> Sculpture { get; set; }
        public virtual DbSet<SculptureType> SculptureType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CareFrequency>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<CareFrequency>()
                .HasMany(e => e.Care)
                .WithRequired(e => e.CareFrequency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CareType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<CareType>()
                .HasMany(e => e.Care)
                .WithRequired(e => e.CareType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DamageLocation>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DamageLocation>()
                .HasMany(e => e.Damages)
                .WithRequired(e => e.DamageLocation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DamageType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DamageType>()
                .HasMany(e => e.Damages)
                .WithRequired(e => e.DamageType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Materials>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Materials>()
                .HasMany(e => e.Sculpture)
                .WithMany(e => e.Materials)
                .Map(m => m.ToTable("SkulptureMaterial").MapLeftKey("Material_Id").MapRightKey("Sculpture_Id"));

            modelBuilder.Entity<MaterialTypes>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Notes>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Notes>()
                .Property(e => e.NotesTxt)
                .IsUnicode(false);

            modelBuilder.Entity<PlacementType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Sculpture>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Sculpture>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Sculpture>()
                .HasMany(e => e.Care)
                .WithRequired(e => e.Sculpture)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sculpture>()
                .HasMany(e => e.Damages)
                .WithRequired(e => e.Sculpture)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sculpture>()
                .HasMany(e => e.Notes)
                .WithRequired(e => e.Sculpture)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sculpture>()
                .HasMany(e => e.SculptureType)
                .WithMany(e => e.Sculpture)
                .Map(m => m.ToTable("SculptureType_Sculpture").MapLeftKey("Sculpture_Id").MapRightKey("SculptureType_Id"));

            modelBuilder.Entity<SculptureType>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }
    }
}
