﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASPMVCConservator;

namespace ASPMVCConservator.Controllers
{
    public class CaresController : Controller
    {
        private ConservatorModel db = new ConservatorModel();

        // GET: Cares
        public ActionResult Index()
        {
            var care = db.Care.Include(c => c.CareFrequency).Include(c => c.CareType).Include(c => c.Sculpture);
            return View(care.ToList());
        }

        // GET: Cares/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Care care = db.Care.Find(id);
            if (care == null)
            {
                return HttpNotFound();
            }
            return View(care);
        }

        // GET: Cares/Create
        public ActionResult Create()
        {
            ViewBag.CareFrequency_Id = new SelectList(db.CareFrequency, "CareFrequency_Id", "Name");
            ViewBag.CareType_Id = new SelectList(db.CareType, "CareType_Id", "Name");
            ViewBag.Sculpture_Id = new SelectList(db.Sculpture, "Sculpture_Id", "Name");
            return View();
        }

        // POST: Cares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Sculpture_Id,CareFrequency_Id,CareType_Id,LastTreatment,NextTreatment,NextSupervision")] Care care)
        {
            if (ModelState.IsValid)
            {
                db.Care.Add(care);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CareFrequency_Id = new SelectList(db.CareFrequency, "CareFrequency_Id", "Name", care.CareFrequency_Id);
            ViewBag.CareType_Id = new SelectList(db.CareType, "CareType_Id", "Name", care.CareType_Id);
            ViewBag.Sculpture_Id = new SelectList(db.Sculpture, "Sculpture_Id", "Name", care.Sculpture_Id);
            return View(care);
        }

        // GET: Cares/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Care care = db.Care.Find(id);
            if (care == null)
            {
                return HttpNotFound();
            }
            ViewBag.CareFrequency_Id = new SelectList(db.CareFrequency, "CareFrequency_Id", "Name", care.CareFrequency_Id);
            ViewBag.CareType_Id = new SelectList(db.CareType, "CareType_Id", "Name", care.CareType_Id);
            ViewBag.Sculpture_Id = new SelectList(db.Sculpture, "Sculpture_Id", "Name", care.Sculpture_Id);
            return View(care);
        }

        // POST: Cares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Sculpture_Id,CareFrequency_Id,CareType_Id,LastTreatment,NextTreatment,NextSupervision")] Care care)
        {
            if (ModelState.IsValid)
            {
                db.Entry(care).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CareFrequency_Id = new SelectList(db.CareFrequency, "CareFrequency_Id", "Name", care.CareFrequency_Id);
            ViewBag.CareType_Id = new SelectList(db.CareType, "CareType_Id", "Name", care.CareType_Id);
            ViewBag.Sculpture_Id = new SelectList(db.Sculpture, "Sculpture_Id", "Name", care.Sculpture_Id);
            return View(care);
        }

        // GET: Cares/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Care care = db.Care.Find(id);
            if (care == null)
            {
                return HttpNotFound();
            }
            return View(care);
        }

        // POST: Cares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Care care = db.Care.Find(id);
            db.Care.Remove(care);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
