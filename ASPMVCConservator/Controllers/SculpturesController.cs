﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASPMVCConservator;

namespace ASPMVCConservator.Controllers
{
    public class SculpturesController : Controller
    {
        private ConservatorModel db = new ConservatorModel();

        // GET: Sculptures
        public ActionResult Index()
        {
            var sculpture = db.Sculpture.Include(s => s.PlacementType).Include(s=>s.SculptureType).Include(s=>s.Materials);
            return View(sculpture.ToList());
        }

        // GET: Sculptures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            } 
            Sculpture sculpture = db.Sculpture.Find(id);
            if (sculpture == null)
            {
                return HttpNotFound();
            }
            return View(sculpture);
        }

        // GET: Sculptures/Create
        public ActionResult Create()
        {
            ViewBag.PlacementType_Id = new SelectList(db.PlacementType, "PlacementType_Id", "Name");
            ViewBag.MaterialType_Id = new SelectList(db.MaterialTypes, "MaterialType_Id", "Name");
            ViewBag.Material_Id = new SelectList(db.Materials, "Material_Id", "Name");
            return View();
        }

        // POST: Sculptures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Sculpture_Id,PlacementType_Id,MaterialType_Id,Material_Id,Name,Address")] Sculpture sculpture, Materials material)
        {
            if (ModelState.IsValid)
            {
                db.Sculpture.Add(sculpture);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PlacementType_Id = new SelectList(db.PlacementType, "PlacementType_Id", "Name", sculpture.PlacementType_Id);
            return View(sculpture);
        }

        // GET: Sculptures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sculpture sculpture = db.Sculpture.Find(id);
            if (sculpture == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlacementType_Id = new SelectList(db.PlacementType, "PlacementType_Id", "Name", sculpture.PlacementType_Id);
            return View(sculpture);
        }

        // POST: Sculptures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Sculpture_Id,PlacementType_Id,Name,Address")] Sculpture sculpture)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sculpture).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PlacementType_Id = new SelectList(db.PlacementType, "PlacementType_Id", "Name", sculpture.PlacementType_Id);
            return View(sculpture);
        }

        // GET: Sculptures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sculpture sculpture = db.Sculpture.Find(id);
            if (sculpture == null)
            {
                return HttpNotFound();
            }
            return View(sculpture);
        }

        // POST: Sculptures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sculpture sculpture = db.Sculpture.Find(id);
            db.Sculpture.Remove(sculpture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
