﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ConservatorWebApi.Models;

namespace ConservatorWebApi.Controllers
{
    public class SculpturesController : ApiController
    {
        private WebApiDbContext db = new WebApiDbContext();

        // GET: api/Sculptures
        public IQueryable<Sculpture> GetSculpture()
        {
            //(return db.Sculpture;
           //var sculpture = db.Sculpture.Include(s => s.Materials).Include(s => s.PlacementType);
            //var sculpture = db.Sculpture.Include(s => s.Materials).ToList();
            //return sculpture.AsQueryable();
            return db.Sculpture.Include(s => s.Materials.Select(p=>p.MaterialTypes))/*.Include(d=>d.Damages.Select(l=>l.DamageType)).Include(d => d.Damages.Select(l => l.DamageLocation)).Include(c=>c.Care.Select(t=>t.CareType)).Include(n=>n.Notes)*/;
        }

        // GET: api/Sculptures/5
        [ResponseType(typeof(Sculpture))]
        public IHttpActionResult GetSculpture(int id)
             //public IQueryable<Materials> GetSculpture(int id)
        {
            Sculpture sculpture = db.Sculpture.Find(id);
            db.Entry(sculpture).Collection(s => s.Materials).Load();
            //db.Entry(sculpture).Reference(s => s.Materials).Load();




            if (sculpture == null)
            {
                return NotFound();
            }


            //if (sculpture == null)
            //{
            //    return null;
            //}

            return Ok(sculpture);


            //return sculpture;
            //return sculpture.Materials.AsQueryable();

        }

        // PUT: api/Sculptures/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSculpture(int id, Sculpture sculpture)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sculpture.Sculpture_Id)
            {
                return BadRequest();
            }

            db.Entry(sculpture).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SculptureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sculptures
        [ResponseType(typeof(Sculpture))]
        public IHttpActionResult PostSculpture(Sculpture sculpture)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Sculpture.Add(sculpture);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sculpture.Sculpture_Id }, sculpture);
        }

        // DELETE: api/Sculptures/5
        [ResponseType(typeof(Sculpture))]
        public IHttpActionResult DeleteSculpture(int id)
        {
            Sculpture sculpture = db.Sculpture.Find(id);
            if (sculpture == null)
            {
                return NotFound();
            }

            db.Sculpture.Remove(sculpture);
            db.SaveChanges();

            return Ok(sculpture);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SculptureExists(int id)
        {
            return db.Sculpture.Count(e => e.Sculpture_Id == id) > 0;
        }
    }
}