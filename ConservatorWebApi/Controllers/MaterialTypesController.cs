﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ConservatorWebApi.Models;

namespace ConservatorWebApi.Controllers
{
    public class MaterialTypesController : ApiController
    {
        private WebApiDbContext db = new WebApiDbContext();

        // GET: api/MaterialTypes
        public IQueryable<MaterialTypes> GetMaterialTypes()
        {
            return db.MaterialTypes;
        }

        // GET: api/MaterialTypes/5
        [ResponseType(typeof(MaterialTypes))]
        public IHttpActionResult GetMaterialTypes(int id)
        {
            MaterialTypes materialTypes = db.MaterialTypes.Find(id);
            if (materialTypes == null)
            {
                return NotFound();
            }

            return Ok(materialTypes);
        }

        // PUT: api/MaterialTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMaterialTypes(int id, MaterialTypes materialTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != materialTypes.MaterialType_Id)
            {
                return BadRequest();
            }

            db.Entry(materialTypes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialTypesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MaterialTypes
        [ResponseType(typeof(MaterialTypes))]
        public IHttpActionResult PostMaterialTypes(MaterialTypes materialTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MaterialTypes.Add(materialTypes);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = materialTypes.MaterialType_Id }, materialTypes);
        }

        // DELETE: api/MaterialTypes/5
        [ResponseType(typeof(MaterialTypes))]
        public IHttpActionResult DeleteMaterialTypes(int id)
        {
            MaterialTypes materialTypes = db.MaterialTypes.Find(id);
            if (materialTypes == null)
            {
                return NotFound();
            }

            db.MaterialTypes.Remove(materialTypes);
            db.SaveChanges();

            return Ok(materialTypes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MaterialTypesExists(int id)
        {
            return db.MaterialTypes.Count(e => e.MaterialType_Id == id) > 0;
        }
    }
}