using System.Runtime.Serialization;

namespace ConservatorWebApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    //[Serializable]
    //[DataContract(IsReference = true)]
    public partial class Materials
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Materials()
        {
            Sculpture = new HashSet<Sculpture>();
        }

        [Key]
        public int Material_Id { get; set; }

        public int? MaterialType_Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public virtual MaterialTypes MaterialTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sculpture> Sculpture { get; set; }
    }
}
