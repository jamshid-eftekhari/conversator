namespace ConservatorWebApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Care")]
    public partial class Care
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Sculpture_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CareFrequency_Id { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CareType_Id { get; set; }

        public DateTime? LastTreatment { get; set; }

        public DateTime? NextTreatment { get; set; }

        public DateTime? NextSupervision { get; set; }

        public virtual CareFrequency CareFrequency { get; set; }

        public virtual CareType CareType { get; set; }

        public virtual Sculpture Sculpture { get; set; }
    }
}
